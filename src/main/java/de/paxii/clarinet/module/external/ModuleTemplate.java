package de.paxii.clarinet.module.external;

import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.Chat;

public class ModuleTemplate extends Module {

  public ModuleTemplate() {
    super("Template", ModuleCategory.OTHER);

    this.setVersion("1.0");
    this.setBuildVersion(18200);
    this.setDescription("A module template.");
  }

  @Override
  public void onEnable() {
    Chat.printClientMessage("Template works!");
  }

}
